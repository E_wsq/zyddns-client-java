# [ZYDDNS](https://gitee.com/qq1302344380/ddns-ali-server)的客户端项目

如果对您有帮助，您可以点右上角 "Star" 支持一下，这样才有继续维护下去的动力，谢谢！

- 目前仅支持阿里云DNS(域名需要在阿里云平台注册购买)

##### 服务端地址: [ZYDDNS](https://gitee.com/qq1302344380/ddns-ali-server)

#### 介绍
ZYDDNS的客户端项目,此项目演示了如何连接服务端,如何定时通过HTTP获取当前的映射信息及更新映射

#### 软件架构
- SpringBoot


#### 安装教程

##### 将项目clone到本地:
``` shell
git clone https://gitee.com/qq1302344380/zyddns-client-java
```

##### 进入项目根目录
``` shell
cd zyddns-client-java
```

##### 修改配置文件:
``` shell
vim ./src/main/resources/application.properties

### ZYDDNS Server的地址(公网IP)
ddns.server.host=http://220.181.38.150:7001
### 要进行DDNS的域名
ddns.host=www.baidu.com

## 执行间隔 单位:毫秒
task.fixedRate=300000

```


##### 使用maven进行打包:
``` shell
mvn clean package
```

##### 运行服务端:
``` shell
java -jar zyddns-client-java-0.0.1-SNAPSHOT.jar

##### ### 后台方式运行
nohup java -jar zyddns-client-java-0.0.1-SNAPSHOT.jar &
```

#### 使用说明

如果有什么建议或在使用过程中出现什么问题,欢迎提交Issues或在下方评论

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

