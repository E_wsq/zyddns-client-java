package club.zykeji.ddns.zyddnsclientjava;

import club.zykeji.ddns.zyddnsclientjava.task.DDNSTask;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import java.util.Timer;

/**
 * @author TheEnd
 */
@SpringBootApplication
public class ZyddnsClientJavaApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(ZyddnsClientJavaApplication.class, args);
        DDNSTask task = context.getBean(DDNSTask.class);
        String fixedRate = context.getEnvironment().getProperty("task.fixedRate");
        new Timer().schedule(task, 0, Integer.parseInt(fixedRate));
    }

}
